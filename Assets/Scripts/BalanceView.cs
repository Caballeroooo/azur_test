﻿using System.Collections;
using TMPro;
using UnityEngine;

public class BalanceView : MonoBehaviour
{ 
    [SerializeField] private TMP_Text _balance;

    private void Start()
    {
        _balance.text = Wallet.Balance.ToString();
        Wallet.WalletValueChanged += SetBalanceValue;
    }

    private void OnDestroy()
    {
        Wallet.WalletValueChanged -= SetBalanceValue;
    }

    private void SetBalanceValue(int value)
    {
        StartCoroutine(SetValue(value));
    }

    private IEnumerator SetValue(int value)
    {
        for (int i = value; i >= 0; i--)
        {
            _balance.text = (Wallet.Balance - i).ToString();
            yield return new WaitForSeconds(0.1f);
        }
    }
}