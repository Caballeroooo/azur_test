using UnityEngine;
using DG.Tweening;

public class Player : MonoBehaviour
{
    [SerializeField] private GameObject _playerBody;
    [SerializeField] private ParticleSystem _waterDrops;
    [SerializeField] private ParticleSystem _waterSpray;

    private void Start()
    {
        GameManager.Instance.FinishEvent += DoFinishAnimation;
    }

    private void OnDestroy()
    {
        GameManager.Instance.FinishEvent -= DoFinishAnimation;
    }

    private void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.layer == LayerMask.NameToLayer("Water"))
        {
            _playerBody.SetActive(false);
            _waterDrops.Play();
            _waterSpray.Play();
            GameManager.Instance.GameOver();
        }
    }

    private void DoFinishAnimation()
    {
        _playerBody.transform.DOJump(_playerBody.transform.position, 1f, 1, 1f).SetLoops(-1);
        
    }
}
