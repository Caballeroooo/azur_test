using UnityEngine;
using DG.Tweening;

public class Coin : MonoBehaviour
{
    [SerializeField] private ParticleSystem _particleCoins;
    [SerializeField] private ParticleSystem _particleSmoke;
    [SerializeField] private GameObject _body;
    
    private void Start()
    {
        InitAnimation();
    }

    private void InitAnimation()
    {
        _body.transform.DORotate(new Vector3(0, 360, 90), 5f, RotateMode.FastBeyond360).SetEase(Ease.Linear).SetLoops(-1, LoopType.Restart);
    }

    private void StartParticles()
    {
        _particleCoins.Play();
        _particleSmoke.Play();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            _body.SetActive(false);
            Wallet.Inc(5);
            StartParticles();
        }
    }
}
