﻿using UnityEngine;
using DG.Tweening;

public class ButtonGlint : MonoBehaviour
{
    [SerializeField] private RectTransform _glint;
    [SerializeField] private RectTransform _glintContainer;
    [SerializeField] private RectTransform _parent;
    [Space]
    [SerializeField] private float _angle;
    [SerializeField] private float _duration;
    [SerializeField] private float _delayTime;
        
    private Sequence _sequence;
    private float _timer;
    private float _offset;

    private bool _sequenceInited;
        
    private void Start()
    {
        InitButtonGlint();
    }
        
    private void FixedUpdate()
    {
        if (!_sequenceInited)
            return;
        
        _timer += Time.deltaTime;

        if (_timer > _delayTime)
        {
            _sequence.Restart();
            _timer = 0;
        }
    }
        
    private void InitButtonGlint()
    {
        _offset = _parent.rect.height + _parent.rect.width;
        SetStartPosition();
    }

    private void SetStartPosition()
    {
        var sequence = DOTween.Sequence();
        sequence.Append(_glintContainer.DOLocalRotate(new Vector3(0, 0, _angle), 0));
        sequence.AppendCallback(() =>
        {
            _glint.sizeDelta = new Vector2 (_parent.rect.width + _parent.rect.height, _glint.sizeDelta.y);
        });
        sequence.Append(_glint.DOLocalMove(Vector3.down * _offset, 0)).OnComplete(DoAnimation);
    }

    private void DoAnimation()
    {
        _sequence = DOTween.Sequence();
        _sequence.SetAutoKill(false);
        _sequence.Append(_glint.DOLocalMove(Vector3.up  * _offset, _duration));
        _sequence.Append(_glint.DOLocalMove(Vector3.down * _offset, 0));
        _sequence.AppendCallback(() => _sequenceInited = true);
    }

    private void OnDestroy()
    {
        _sequence.Kill();
    }
}