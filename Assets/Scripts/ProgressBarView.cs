using DG.Tweening;
using UnityEngine;

public class ProgressBarView : MonoBehaviour
{
    [SerializeField] private RectTransform _barFill;
    [SerializeField] private RectTransform _barBackground;

    private float _distance;

    public void InitBar(float distance)
    {
        gameObject.SetActive(true);
        _distance = distance;
        _barFill.DOLocalMoveX(-_barBackground.rect.width, 0);
    }
    
    public void SetBarValue(float value)
    {
        var targetPosX = value * _barBackground.rect.width / _distance;
        _barFill.DOLocalMoveX(targetPosX - _barBackground.rect.width, 0);
    }
}
