using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public event Action StartButtonClick;
    public event Action FinishEvent;
    public event Action GameOverEvent;

    public static GameManager Instance;

    private void Awake()
    {
        Instance = this;
    }

    public void OnStartButtonClick()
    {
        StartButtonClick?.Invoke();
    }
    
    public void OnRestartButtonClick()
    {
        SceneManager.LoadScene(0);
    }

    public void Finish()
    {
        FinishEvent?.Invoke();
    }
    
    public void GameOver()
    {
        GameOverEvent?.Invoke();
    }
}
