﻿using System;
using Random = UnityEngine.Random;

public static class Wallet
{
    public static event Action<int> WalletValueChanged;
    public static int Balance { get; private set; } = Random.Range(0, 151);

    public static void Inc(int value)
    {
        Balance += value;
        WalletValueChanged?.Invoke(value);
    }
    
    public static void Dec(int value)
    {
        Balance -= value;
        WalletValueChanged?.Invoke(value);
    }
}