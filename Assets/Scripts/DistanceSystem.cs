﻿using UnityEngine;

public class DistanceSystem : MonoBehaviour
{
    [SerializeField] private Transform _playerTransform;
    [SerializeField] private Transform _targetTransform;
    [SerializeField] private ProgressBarView _progressBarView;

    private bool _isStart;

    private float _startPlayerPosZ;

    private void Start()
    {
        _startPlayerPosZ = _playerTransform.position.z;
        GameManager.Instance.StartButtonClick += Init;
    }
    
    private void Update()
    {
        if (!_isStart)
            return;
        
        _progressBarView.SetBarValue(GetCurrentPos());
    }

    private void OnDestroy()
    {
        GameManager.Instance.StartButtonClick -= Init;
    }

    private void Init()
    {
        _progressBarView.InitBar(Mathf.Abs(_targetTransform.position.z - _playerTransform.position.z));
        _isStart = true;
    }

    private float GetCurrentPos()
    {
        if (_startPlayerPosZ < 0)
        {
            return _playerTransform.position.z < 0
                ? Mathf.Abs(_startPlayerPosZ - _playerTransform.position.z)
                : _playerTransform.position.z + Mathf.Abs(_startPlayerPosZ);
        }
        else
        {
            return _playerTransform.position.z - _startPlayerPosZ;
        }
    }
}