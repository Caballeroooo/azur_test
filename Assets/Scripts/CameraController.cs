using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private Animator _animator;

    private void Start()
    {
        GameManager.Instance.StartButtonClick += PlayStartAnimation;
        GameManager.Instance.FinishEvent += PlayFinishAnimation;
    }

    private void OnDestroy()
    {
        GameManager.Instance.StartButtonClick -= PlayStartAnimation;
        GameManager.Instance.FinishEvent -= PlayFinishAnimation;
    }

    private void PlayStartAnimation()
    {
        _animator.SetBool("IsStart", true);
    }
    
    private void PlayFinishAnimation()
    {
        _animator.SetBool("IsFinish", true);
    }
}
