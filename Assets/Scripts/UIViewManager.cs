﻿using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIViewManager : MonoBehaviour
{
    [SerializeField] private Button _startButton;
    [SerializeField] private Button _restartButton;
    [SerializeField] private GameObject _gameOverText;
    [SerializeField] private GameObject _finishText;
    [SerializeField] private GameObject _balancePanel;
    [SerializeField] private TMP_Text _gameOverTitle;

    private void Start()
    {
        _startButton.gameObject.SetActive(true);
        _balancePanel.SetActive(true);
        GameManager.Instance.StartButtonClick += SetStartState;
        GameManager.Instance.GameOverEvent += SetGameOverState;
        GameManager.Instance.FinishEvent += SetFinishState;
    }

    private void OnDestroy()
    {
        GameManager.Instance.StartButtonClick -= SetStartState;
        GameManager.Instance.GameOverEvent -= SetGameOverState;
        GameManager.Instance.FinishEvent -= SetFinishState;
    }

    private void SetStartState()
    {
        _startButton.gameObject.SetActive(false);
    }

    private void SetGameOverState()
    {
        _restartButton.gameObject.SetActive(true);
        _gameOverText.SetActive(true);
        DoTitleAnimation();
    }
    
    private void SetFinishState()
    {
        _restartButton.gameObject.SetActive(true);
        _finishText.SetActive(true);
    }

    private void DoTitleAnimation()
    {
        _gameOverTitle.rectTransform.DOScale(0, 0);
        _gameOverTitle.rectTransform.DOScale(1, 0.3f).SetEase(Ease.InOutElastic);
    }
}