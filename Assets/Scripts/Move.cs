using UnityEngine;

public class Move : MonoBehaviour
{
    [SerializeField] private Animator _animator;
    [SerializeField] private float _speed = 10f;

    private bool _canMove;
    
    private void Start()
    {
        GameManager.Instance.StartButtonClick += SetMovable;
        GameManager.Instance.FinishEvent += SetFinishState;
        GameManager.Instance.GameOverEvent += SetFinishState;
    }

    private void Update()
    {
        if (!_canMove)
            return;
        
        if (Input.GetMouseButton(0))
        {
            DoMove();
        }
        else
        {
            _animator.SetBool("IsRun", false);
        }
    }

    private void OnDestroy()
    {
        GameManager.Instance.StartButtonClick -= SetMovable;
        GameManager.Instance.FinishEvent -= SetFinishState;
        GameManager.Instance.GameOverEvent -= SetFinishState;
    }

    private void DoMove()
    {
        var mX = Input.GetAxis("Mouse X") / 10;
        transform.Translate(mX, 0, 0);
        transform.Translate(Vector3.forward * _speed * Time.deltaTime);
        _animator.SetBool("IsRun", true);
    }

    private void SetMovable()
    {
        _canMove = true;
    }

    private void SetFinishState()
    {
        _canMove = false;
        _animator.SetBool("IsRun", false);
    }
}
